package com.example.notforgot;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface Api {
    @POST("login")
    Call<String> Login(@Body LoginForm body);

    @POST("register")
    Call<RegistrationResponceForm> Registration(@Body RegistrationForm body);
}
