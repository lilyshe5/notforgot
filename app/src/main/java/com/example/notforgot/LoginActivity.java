package com.example.notforgot;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        context = this;

        //Обработать нажатие на кнопку входа
    }

    private Boolean isValidEmail(String email) {
        //Проверить, не пустой ли поле почты. Если поле почты пустой, то вернуть false, иначе true
        return true;
    }

    private Boolean isValidPassword(String password) {
        //Проверить, не пустое ли поле пароль. Если пароль пустой, то вернуть false, иначе true
        return true;
    }

    //Тут ничего трогать не нужно :)
    private void Login(String email, String password) {
        ResponseController.getInstance().getApi().Login(new LoginForm(email, password)).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                startActivity(new Intent(context, MainActivity.class));
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG).show();
            }
        });
    }
}