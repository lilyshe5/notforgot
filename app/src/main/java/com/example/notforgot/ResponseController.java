package com.example.notforgot;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ResponseController {

    static final String BASE_URL = "http://practice.mobile.kreosoft.ru/api/";
    private static ResponseController mInstance;

    private Api api;
    private Retrofit retrofit;

    public static ResponseController getInstance() {
        if (mInstance == null)
            mInstance = new ResponseController();

        return mInstance;
    }

    public ResponseController() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();


        api = retrofit.create(Api.class);
    }

    public Api getApi() {
        return api;
    }
}