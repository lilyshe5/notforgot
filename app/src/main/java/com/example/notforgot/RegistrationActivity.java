package com.example.notforgot;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends AppCompatActivity {

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        context = this;
    }

    private Boolean isValidEmail(String email) {
        //Проверить, не пустой ли поле почты. Если поле почты пустое, то вернуть false, иначе true
        return true;
    }

    private Boolean isValidPassword(String password) {
        //Проверить, не пустой ли поле пароля. Если пароль пустой, то вернуть false, иначе true
        return true;
    }

    private Boolean isValidName(String name) {
        //Проверить, не пустой ли поле имени. Если имя пустое то вернуть false, иначе true
        return true;
    }

    private Boolean isValidConfirmPassowrd(String confirmPassword) {
        //Проверить, не пустой ли поле подтверждения пароя. Если подтверждение пароя пустое, то вернуть false, иначе true
        return true;
    }

    private void Registration(String email, String name, String password) {
        ResponseController.getInstance().getApi().Registration(new RegistrationForm(email, name, password)).enqueue(new Callback<RegistrationResponceForm>() {
            @Override
            public void onResponse(Call<RegistrationResponceForm> call, Response<RegistrationResponceForm> response) {
                Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_LONG).show();
                startActivity(new Intent(context, LoginActivity.class));
                finish();
            }

            @Override
            public void onFailure(Call<RegistrationResponceForm> call, Throwable t) {
                Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG).show();
            }
        });
    }
}