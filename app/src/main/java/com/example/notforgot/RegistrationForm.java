package com.example.notforgot;

import com.google.gson.annotations.SerializedName;

public class RegistrationForm{
    @SerializedName("email")
    private String email;

    @SerializedName("name")
    private String name;

    @SerializedName("password")
    private String password;

    public RegistrationForm(String email, String name, String password){
        this.email = email;
        this.password = password;
        this.name = name;
    }
}