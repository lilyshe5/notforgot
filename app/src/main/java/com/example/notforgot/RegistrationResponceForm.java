package com.example.notforgot;

public class RegistrationResponceForm {
    private String email;
    private String name;
    private Integer id;
    private String token;

    public RegistrationResponceForm(String email, String name, Integer id, String token){
        this.email = email;
        this.id = id;
        this.name = name;
        this.token = token;
    }
}